<!doctype html>
<html <?php language_attributes(); ?>>
  <head>
    <!-- Required meta tags -->
    <meta charset="<?php bloginfo('charset'); ?>">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <?php wp_head();?>
    <style>
        .service-inner .card-header[aria-expanded="true"]::after, .service-inner .card-header[aria-expanded="false"]::after{
            content: url(<?php echo get_theme_file_uri('icons/_UP.svg') ?>);
        }
        .service-inner .card-header[aria-expanded="false"]::after{
            content: url(<?php echo get_theme_file_uri('icons/_DOWN.svg') ?>);
        }
    </style>
  </head>
  <body>
      <header <?php body_class(); ?>>
        <div class="container">
            <nav class="menu navbar navbar-expand-lg ">
                <a class="menu-logo navbar-brand" href="<?php echo site_url()?>"><img src="<?php echo get_theme_file_uri('img/logo.png') ?>" alt="Logo"></a>
                
                <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
                  <span class="navbar-toggler-icon"><img src="<?php echo get_theme_file_uri('icons/menu-button.svg') ?>" alt=""></span>
                </button>
              
                <div class="menu-content collapse navbar-collapse" id="navbarSupportedContent">
                  <ul class="navbar-nav">
                    <li class="nav-item active" >
                      <a class="nav-link" href="<?php echo site_url('/blog')?>">Blog<span class="sr-only">(current)</span></a>
                    </li>
                    <li class="nav-item active" >
                      <a class="nav-link" href="<?php echo site_url('#service')?>">Service</a>
                    </li>
                    <li class="nav-item active" >
                      <a class="nav-link" href="<?php echo site_url('#team')?>">Team</a>
                    </li>
                    <li class="cart nav-item active" >
                      <a class="nav-link" href="#"><img src="<?php echo get_theme_file_uri('icons/SHOPPING_CART.svg') ?>" alt="cart"></a>
                    </li>
                    <li class="serch nav-item active">
                      <a class="nav-link" href="#"><img src="<?php echo get_theme_file_uri('icons/MAGNIFYING_GLASS.svg') ?>" alt="search"></a>  
                    </li>
                  </ul>
                  <!-- <form class="form-inline my-2 my-lg-0">
                    <input class="form-control mr-sm-2" type="search" placeholder="Search" aria-label="Search">
                    <button class="btn btn-link" type="submit">Search</button>
                  </form> -->
                </div>
              </nav>
            </div>    
      </header>