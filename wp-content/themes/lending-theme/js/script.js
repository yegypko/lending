$(document).ready(function(){
    $('.slider-carusel').slick({
     
            infinite: true,
            slidesToShow: 1,
            slidesToScroll: 1,
            dots: false,
            // speed: 1000,
            // autoplay: true,
            // autoplaySpeed: 1000,
//          adaptiveHeight: true
            // fade: true,
            // cssEase: 'linear',
            arrows: false,
            asNavFor: '.slider-content',
            responsive: [
                {
                    breakpoint: 767,
                    settings: {
                        slidesToShow: 1,
                        slidesToScroll: 1,
                        infinite: true,
                        dots: false,
                        arrows: false,
                        autoplay: true
                    }
                }
            ]
         
    });

    $('.slider-content').slick({
        slidesToShow: 3,
        slidesToScroll: 1,
        asNavFor: '.slider-carusel',
        dots: false,
        centerMode: true,
        focusOnSelect: true,
        arrows: false,
        responsive: [
            {
                breakpoint: 767,
                settings: "unslick"
            }
        ]
        
      });
  });