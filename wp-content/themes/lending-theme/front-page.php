<?php get_header(); ?>
<section class="slider">
              <!-- Slider 1 -->
              <div class="container-fluid">
               
                  <div class="slider-carusel">
                    <!-- inner 1 -->
                    <?php 
				
                        $frontpageSlider = new WP_Query (array(
                            // Post & Page Parameters
                            'posts_per_page'=> -1,
                            'post_type'   => 'slider',
                        ));
                                while($frontpageSlider->have_posts()){
                                    $frontpageSlider->the_post('slider');?>
                    <div class="slider-carusel-inner">
                      <div class="inner">
                        <H2><?php the_title();?></H2>
                        <h1><?php the_field('slider_content');?></h1>
                        <hr>
                        <button>Learn more</button>
                      </div>
                      <img src="<?php $SliderImage = get_field('slider_image'); echo $SliderImage['sizes']['SliderImageMain']?>" alt="slider">
                    </div>

                    <?php }wp_reset_postdata();?>
                    
               
              </div>
              <!-- Slider 2 -->
              <div class="container">
                  <div class="slider-content">
                  <?php 
				
                $frontpageSlider = new WP_Query (array(
                    // Post & Page Parameters
                    'posts_per_page'=> -1,
                    'post_type'   => 'slider',
                ));
                        while($frontpageSlider->have_posts()){
                            $frontpageSlider->the_post('slider');?>
                    <p><span><?php the_field('slider_number');?></span><?php the_field('slider_page');?></p>
                    <?php }wp_reset_postdata();?>
                  </div>
              </div>
      </section>
      
      <section id="blog" class="blog">
        <div class="container">
            <div class="section-name">
              <h3>Our stories</h3>
              <h4>Latest blog</h4>
              <hr class="hr-name">
            </div>
            
            <div class="blog-inner">
              <div class="row">
              <?php 
				
				$frontpagePosts = new WP_Query (array(
					// Post & Page Parameters
                    'posts_per_page'=> 3,
                    'post_type'   => 'post',
				));
                        while($frontpagePosts->have_posts()){
                            $frontpagePosts->the_post('post');?>

                        <div class="col-xl-4 col-lg-4 col-md-6 col-sm-12 post">
                        
                        <div class="post-cover">
                            <div class="picture">
                            <div class="post-date">
                                <span><?php the_date('d')?></span>
                                <p><?php echo get_the_date('M')?></p>
                            </div>
                            <img src="<?php $PostImage = get_field('post_thumb'); echo $PostImage['sizes']['PostImage']?>" alt="cover"/>
                            </div>
                            
                            <div class="post-header">
                            <a href="<?php the_permalink();?>"><?php the_title();?></a>
                            </div>
                            <p><?php echo wp_trim_words(get_the_content(), '12');?></p>
                            <hr>
                            <div class="post-contact">
                            <a href="<?php the_permalink();?>"><img src="<?php echo get_theme_file_uri('icons/VIEW.svg') ?>"/><span>542</span></a>
                            <a href="<?php the_permalink();?>"><img src="<?php echo get_theme_file_uri('icons/SPEECH_BUBBLE.svg') ?>"/><span>17</span></a>
                            </div>
                        </div>
                        </div>

                <?php }wp_reset_postdata();?>
              </div>
            </div>
        </div>
      </section>

      <section id="service" class="service">
        <div class="container">
          <div class="section-name">
          <?php 
				
				$frontpageServices = new WP_Query (array(
					// Post & Page Parameters
                    'posts_per_page'=> -1,
                    'post_type'   => 'services',
				));
                        while($frontpageServices->have_posts()){
                            $frontpageServices->the_post('services');?>
            <h3><?php the_title();?></h3>
            <h4><?php the_field('services_title');?></h4>
            <hr class="hr-name">
            <p><?php the_field('services_content');?></p>

            <div class="service-inner">
              <div class="row">
                  <div class="col-lg-6 col-md-6 mt-5">
                    <div class="service-img">
                    <img src="<?php $ServiceImage = get_field('services_image'); echo $ServiceImage['sizes']['ServiceImageMain']?>"/>
                    </div>
                  </div>
                  <div class="col-lg-6 col-md-6 mt-5">
                    <div id="service-accordion">
                      <div class="card">
                        <div class="card-header" id="service-heading-4" data-toggle="collapse" data-target="#service-collapse-4" aria-expanded="true" aria-controls="service-collapse-4">
                           <img src="<?php echo get_theme_file_uri('icons/PICTURE.svg') ?>"/>
                           <?php the_field('services_title_1');?>
                        </div>
                        <div id="service-collapse-4" class="collapse show" aria-labelledby="service-heading-4" data-parent="#service-accordion">
                          <div class="card-body">
                          <?php the_field('services_content_1');?>
                          </div>
                        </div>
                      </div>
                      <div class="card">
                        <div class="card-header" id="service-heading-5" data-toggle="collapse" data-target="#service-collapse-5" aria-expanded="false" aria-controls="service-collapse-5">
                          <img src="<?php echo get_theme_file_uri('icons/EQUALIZER.svg') ?>"/>
                          <?php the_field('services_title_2');?>
                        </div>
                        <div id="service-collapse-5" class="collapse" aria-labelledby="service-heading-5" data-parent="#service-accordion">
                          <div class="card-body">
                          <?php the_field('services_content_2');?>
                          </div>
                        </div>
                      </div>
                      <div class="card">
                        <div class="card-header" id="service-heading-6" data-toggle="collapse" data-target="#service-collapse-6" aria-expanded="false" aria-controls="service-collapse-6">
                          <img src="<?php echo get_theme_file_uri('icons/BULLSEYE.svg') ?>"/>
                          <?php the_field('services_title_3');?>
                        </div>
                        <div id="service-collapse-6" class="collapse" aria-labelledby="service-heading-6" data-parent="#service-accordion">
                          <div class="card-body">
                          <?php the_field('services_content_3');?>
                          </div>
                        </div>
                      </div>
                    </div>
                  </div>
              </div>
            </div>
            <?php }wp_reset_postdata();?>
          </div>
        </div>
      </section>
      
      <section id="team" class="team">
        <div class="container">
          <div class="section-name">
                <h3>Who we are</h3>
                <h4>MEET OUR TEAM</h4>
                <hr class="hr-name">
                <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat.</p>
                <div class="row">
                        <?php 
                                
                                $frontpageTeam = new WP_Query (array(
                                    // Post & Page Parameters
                                    'posts_per_page'=> -1,
                                    'post_type'   => 'team',
                                ));
                                        while($frontpageTeam->have_posts()){
                                            $frontpageTeam->the_post('team');?>
                            
                            <div class="col-xl-4 col-lg-4 col-md-6 col-sm-12 team-inner">
                            
                                <div class="team-hover">
                                    <img class="item" src="<?php $TeamImage = get_field('team_img'); echo $TeamImage['sizes']['TeamImageMain']?>" alt="team">
                                
                                    <ul class="team-social">
                                        <li><a href="<?php the_field('facebook_link');?>"><img class="social" src="<?php echo get_theme_file_uri('icons/social/facebook.svg') ?>" alt="facebook"></a></li>
                                        <li><a href="<?php the_field('twitter_link');?>"><img class="social" src="<?php echo get_theme_file_uri('icons/social/twitter.svg') ?>" alt="twitter"></a></li>
                                        <li><a href="<?php the_field('pinterest_link');?>"><img class="social" src="<?php echo get_theme_file_uri('icons/social/pinterest.svg') ?>" alt="pinterest"></a></li>
                                        <li><a href="<?php the_field('instagram_link');?>"><img class="social" src="<?php echo get_theme_file_uri('icons/social/instagram.svg') ?>" alt="instagram"></a></li>
                                    </ul>
                                    
                                </div>
                            
                                <h6><?php the_title();?></h6>
                                <span><?php the_field('teame_jobe');?></span>
                            </div>
                            <?php }wp_reset_postdata();?>
                </div>
          </div>
        </div>
      </section>


<?php get_footer(); ?>