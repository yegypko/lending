<?php get_header();?>

    <section id="blog" class="blog">
        <div class="container">
            <div class="section-name">
              <h4><?php the_archive_title();?></h4>
              <h3><?php the_archive_description(); ?></h3>
              <hr class="hr-name">
            </div>
            
            <div class="blog-inner">
              <div class="row">
                    <?php
                        while(have_posts()){
                        the_post();?>
                        <div class="col-xl-12 col-lg-12 col-md-12 col-sm-12 post">
                        
                        <div class="post-cover">
                            <span><?php the_date('d')?></span>
                            <p><?php echo get_the_date('M')?></p>
                            <div class="post-header">
                            <a href="<?php the_permalink();?>"><?php the_title();?></a>
                            </div>
                            <p><?php the_content();?></p>
                            <hr>
                        </div>
                        </div>
                    <?php }?>
                
                    <h4><?php echo paginate_links(); ?></h4>
              </div>
            </div>
        </div>
      </section>





<?php get_footer(); ?>