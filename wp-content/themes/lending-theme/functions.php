<?php 

function lending_files() {
    wp_enqueue_script('lending-jquery-js', 'https://code.jquery.com/jquery-3.3.1.slim.min.js');
    wp_enqueue_script('lending-popper-js', 'https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.3/umd/popper.min.js');
    wp_enqueue_script('lending-bootstrap-js', 'https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/js/bootstrap.min.js');
    wp_enqueue_script('lending-slick-js', get_theme_file_uri('/js/slick.min.js'), NULL, '1.0', true);
    wp_enqueue_script('lending-main-js', get_theme_file_uri('/js/script.js'), NULL, microtime(), true);
    
    wp_enqueue_style('custom-google-fonts-kaushan', 'https://fonts.googleapis.com/css?family=Kaushan+Script&display=swap');
    wp_enqueue_style('custom-google-fonts-montserrat', 'https://fonts.googleapis.com/css?family=Montserrat&display=swap');
    wp_enqueue_style('custom-google-fonts-roboto', 'https://fonts.googleapis.com/css?family=Roboto:100,100i,300,300i,400,400i,500,500i,700,700i,900,900i&display=swap');
    wp_enqueue_style('lending-bootstrap_styles', 'https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/css/bootstrap.min.css');
    wp_enqueue_style('lending-main_styles', get_stylesheet_uri(), NULL, microtime());
    
}

add_action('wp_enqueue_scripts', 'lending_files');

function lending_features() {
	// register_nav_menu('headerMenuLocation', 'Header Menu Location');
	// register_nav_menu('footerLocationOne', 'Footer Location One');
	// register_nav_menu('footerLocationTwo', 'Footer Location Two');
    add_theme_support('title-tag');
    // add_theme_support('post-thumbnails');
    // if use hard cod crop - add_image_size('professorLandscape', 400, 260, array('left', 'top'));
    add_image_size('PostImage', 350, 240, true);
    add_image_size('ServiceImageMain', 540, 360, true);
    add_image_size('TeamImageMain', 350, 433, true);
    add_image_size('SliderImageMain', 1920, 1000, true);
}

add_action('after_setup_theme', 'lending_features');