<?php get_header(); ?>
<section class="slider">
              <!-- Slider 1 -->
              <div class="container-fluid">
                <!-- <div class="row"> -->
                  <div class="slider-carusel">
                    <!-- inner 1 -->
                    <div class="slider-carusel-inner">
                      <div class="inner">
                        <H2>Creative Template</H2>
                        <h1>Welcome<br>
                        to MoGo</h1>
                        <hr>
                        <button>Learn more</button>
                      </div>
                      <img src="<?php echo get_theme_file_uri('img/slider-1.jpg') ?>" alt="slider-1">
                    </div>
                     <!-- inner 2 -->
                     <div class="slider-carusel-inner">
                      <div class="inner">
                        <H2>Creative Template</H2>
                        <h1>Welcome<br>
                        to MoGo</h1>
                        <hr>
                        <button>Learn more</button>
                      </div>
                      <img src="<?php echo get_theme_file_uri('img/slider-1.jpg') ?>" alt="slider-1">
                    </div>
                     <!-- inner 3 -->
                     <div class="slider-carusel-inner">
                      <div class="inner">
                        <H2>Creative Template</H2>
                        <h1>Welcome<br>
                        to MoGo</h1>
                        <hr>
                        <button>Learn more</button>
                      </div>
                      <img src="<?php echo get_theme_file_uri('img/slider-1.jpg') ?>" alt="slider-1">
                    </div>
                  </div>
                <!-- </div> -->
              </div>
              <!-- Slider 2 -->
              <div class="container">
                  <div class="slider-content">
                    <p><span>01</span> Blog</p>
                    <p><span>02</span> Work</p>
                    <p><span>03</span> Tem</p>
                    <!-- <p><span>04</span> Contacts</p> -->
                  </div>
              </div>
      </section>
      
      <section id="blog" class="blog">
        <div class="container">
            <div class="section-name">
              <h3>Our stories</h3>
              <h4>Latest blog</h4>
              <hr class="hr-name">
            </div>
            
            <div class="blog-inner">
              <div class="row">
                <div class="col-xl-4 col-lg-4 col-md-6 col-sm-12 post">
                  
                  <div class="post-cover">
                    <div class="picture">
                      <div class="post-date">
                        <span>15</span>
                        <p>Jan</p>
                      </div>
                      <img src="<?php echo get_theme_file_uri('img/blog-1.jpg') ?>" alt="cover image"/>
                    </div>
                    
                    <div class="post-header">
                      <a href="#">Lorem ipsum dolor sit amet</a>
                    </div>
                    <p>Consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.</p>
                    <hr>
                    <div class="post-contact">
                      <a href="#"><img src="<?php echo get_theme_file_uri('icons/VIEW.svg') ?>"/><span>542</span></a>
                      <a href="#"><img src="<?php echo get_theme_file_uri('icons/SPEECH_BUBBLE.svg') ?>"/><span>17</span></a>
                    </div>
                  </div>
                </div>
                <div class="col-xl-4 col-lg-4 col-md-6 col-sm-12 post">
                  
                  <div class="post-cover">
                    <div class="picture">
                      <div class="post-date">
                        <span>14</span>
                        <p>Jan</p>
                      </div>
                      <img src="<?php echo get_theme_file_uri('img/blog-2.jpg') ?>" alt="cover image"/>
                    </div>
                    
                    <div class="post-header">
                      <a href="#">sed do eiusmod tempor</a>
                    </div>
                    <p>Adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.</p>
                    <hr>
                    <div class="post-contact">
                      <a href="#"><img src="<?php echo get_theme_file_uri('icons/VIEW.svg') ?>"/><span>992</span></a>
                      <a href="#"><img src="<?php echo get_theme_file_uri('icons/SPEECH_BUBBLE.svg') ?>"/><span>42</span></a>
                    </div>
                  </div>
                </div>
                <div class="col-xl-4 col-lg-4 col-md-6 col-sm-12 post">
                  
                  <div class="post-cover">
                    <div class="picture">
                      <div class="post-date">
                        <span>12</span>
                        <p>Jan</p>
                      </div>
                      <img src="<?php echo get_theme_file_uri('img/blog-3.jpg') ?>" alt="cover image"/> 
                    </div>
                    
                    <div class="post-header">
                      <a href="#">incididunt ut labore et dolore</a>
                    </div>
                    <p>Elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.</p>
                    <hr>
                    <div class="post-contact">
                      <a href="#"><img src="<?php echo get_theme_file_uri('icons/VIEW.svg') ?>"/><span>1560</span></a>
                      <a href="#"><img src="<?php echo get_theme_file_uri('icons/SPEECH_BUBBLE.svg') ?>"/><span>98</span></a>
                    </div>
                  </div>
                </div>
              </div>
            </div>
        </div>
      </section>

      <section id="service" class="service">
        <div class="container">
          <div class="section-name">
            <h3>Service</h3>
            <h4>what we do</h4>
            <hr class="hr-name">
            <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. </p>

            <div class="service-inner">
              <div class="row">
                  <div class="col-lg-6 col-md-6 mt-5">
                    <div class="service-img">
                      <img src="<?php echo get_theme_file_uri('img/service.jpg') ?>"/>
                    </div>
                  </div>
                  <div class="col-lg-6 col-md-6 mt-5">
                    <div id="service-accordion">
                      <div class="card">
                        <div class="card-header" id="service-heading-4" data-toggle="collapse" data-target="#service-collapse-4" aria-expanded="true" aria-controls="service-collapse-4">
                           <img src="<?php echo get_theme_file_uri('icons/PICTURE.svg') ?>"/>
                          Photography
                        </div>
                        <div id="service-collapse-4" class="collapse show" aria-labelledby="service-heading-4" data-parent="#service-accordion">
                          <div class="card-body">
                            Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.
                            Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.
                          </div>
                        </div>
                      </div>
                      <div class="card">
                        <div class="card-header" id="service-heading-5" data-toggle="collapse" data-target="#service-collapse-5" aria-expanded="false" aria-controls="service-collapse-5">
                          <img src="<?php echo get_theme_file_uri('icons/EQUALIZER.svg') ?>"/>
                          creativity
                        </div>
                        <div id="service-collapse-5" class="collapse" aria-labelledby="service-heading-5" data-parent="#service-accordion">
                          <div class="card-body">
                            Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.
                          </div>
                        </div>
                      </div>
                      <div class="card">
                        <div class="card-header" id="service-heading-6" data-toggle="collapse" data-target="#service-collapse-6" aria-expanded="false" aria-controls="service-collapse-6">
                          <img src="<?php echo get_theme_file_uri('icons/BULLSEYE.svg') ?>"/>
                          web design
                        </div>
                        <div id="service-collapse-6" class="collapse" aria-labelledby="service-heading-6" data-parent="#service-accordion">
                          <div class="card-body">
                            Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.
                          </div>
                        </div>
                      </div>
                    </div>
                  </div>
              </div>
            </div>
          </div>
        </div>
      </section>
      
      <section id="team" class="team">
        <div class="container">
          <div class="section-name">
            <h3>Who we are</h3>
            <h4>Meet our team</h4>
            <hr class="hr-name">
            <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. </p>
          <div class="row">
            <div class="col-xl-4 col-lg-4 col-md-6 col-sm-12 team-inner">
              
              <div class="team-hover">
                <img class="item" src="<?php echo get_theme_file_uri('img/team-1.jpg') ?>" alt="team">
            
                  <ul class="team-social">
                  <li><a href="#"><img class="social" src="<?php echo get_theme_file_uri('icons/social/facebook.svg') ?>" alt="facebook"></a></li>
                    <li><a href="#"><img class="social" src="<?php echo get_theme_file_uri('icons/social/twitter.svg') ?>" alt="twitter"></a></li>
                    <li><a href="#"><img class="social" src="<?php echo get_theme_file_uri('icons/social/pinterest.svg') ?>" alt="pinterest"></a></li>
                    <li><a href="#"><img class="social" src="<?php echo get_theme_file_uri('icons/social/instagram.svg') ?>" alt="instagram"></a></li>
                  </ul>
                
              </div>
              
              <h6>Matthew Dix</h6>
              <span>Graphic Design</span>

            </div>
            <div class="col-xl-4 col-lg-4 col-md-6 col-sm-12 team-inner">
              <div class="team-hover">
              <img class="item" src="<?php echo get_theme_file_uri('img/team-2.jpg') ?>" alt="team">
            
                  <ul class="team-social">
                  <li><a href="#"><img class="social" src="<?php echo get_theme_file_uri('icons/social/facebook.svg') ?>" alt="facebook"></a></li>
                    <li><a href="#"><img class="social" src="<?php echo get_theme_file_uri('icons/social/twitter.svg') ?>" alt="twitter"></a></li>
                    <li><a href="#"><img class="social" src="<?php echo get_theme_file_uri('icons/social/pinterest.svg') ?>" alt="pinterest"></a></li>
                    <li><a href="#"><img class="social" src="<?php echo get_theme_file_uri('icons/social/instagram.svg') ?>" alt="instagram"></a></li>
                  </ul>
                
              </div>
              
              <h6>Christopher Campbell</h6>
              <span>Branding/UX design</span>

            </div>
            <div class="col-xl-4 col-lg-4 col-md-6 col-sm-12 team-inner">
              <div class="team-hover">
              <img class="item" src="<?php echo get_theme_file_uri('img/team-3.jpg') ?>" alt="team">
            
                  <ul class="team-social">
                    <li><a href="#"><img class="social" src="<?php echo get_theme_file_uri('icons/social/facebook.svg') ?>" alt="facebook"></a></li>
                    <li><a href="#"><img class="social" src="<?php echo get_theme_file_uri('icons/social/twitter.svg') ?>" alt="twitter"></a></li>
                    <li><a href="#"><img class="social" src="<?php echo get_theme_file_uri('icons/social/pinterest.svg') ?>" alt="pinterest"></a></li>
                    <li><a href="#"><img class="social" src="<?php echo get_theme_file_uri('icons/social/instagram.svg') ?>" alt="instagram"></a></li>
                  </ul>
                
              </div>
              
              <h6>Michael Fertig </h6>
              <span>Developer</span>

            </div>
          </div>
        </div>
      </section>


<?php get_footer(); ?>